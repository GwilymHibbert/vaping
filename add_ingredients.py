def saveIngredient(ingredientData):
    ingredientFile=open("ingredients.txt","r")
    ingredientLine=ingredientFile.readline()
    saveData=""
    while ingredientLine:
        saveData=saveData+ingredientLine
        ingredientLine=ingredientFile.readline()
    #print(saveData)
    ingredientFile.close()
    saveFile=open("ingredients.txt","w")
    confirm=stringInput("Confirm that the ingredient was entered correctly and you wish to save y/n\n",["y","n"])
    if confirm=="y":
        saveFile.write(saveData+ingredientData)
    else:
        saveFile.write(saveData)
    saveFile.close()

def gatherData():
    name=input("please enter name of ingredient\n")
    category=stringInput("""please enter category of ingredient to be
    added nicotine concentrate or flavour\n""",["nicotine concentrate","flavour"])
    if category=="nicotine concentrate":
        nicotineConc=numberInput("please enter nicotine concentration in mg/ml\n")
    else:
        nicotineConc="0"
    vgConc=numberInput("please enter VG concentration of ingredient\n")
    cost=numberInput("please enter the cost of the ingredient per ml\n")
    #print(category,vgConc,nicotineConc,name,cost)
    data=("\n"+category+","+vgConc+","+nicotineConc+","+name+","+cost)
    return(data)
    
def stringInput(text,list):
    text=input(text)
    while text not in list:
        text=input("invalid selection "+text)
    return(text)


def isNumber(s):
    try:
        float(s)
        return True
    except ValueError:
        return False

def numberInput(text):
    number=input(text)
    test=isNumber(number)
    while test==False:
        number=input("invalid entry "+text)
        test=isNumber(number)
    return(number)
    
def main():
    data=gatherData()
    saveIngredient(data)
main()
