
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 18 15:47:43 2016

@author: don-quixote
"""

import sys
from tkinter import *
from tkinter import ttk
from tkinter import messagebox

def getIngredients(category=0):
    ingredientsFile=open("ingredients.txt","r")
    ingredientsLine=ingredientsFile.readline()
    ingredientList=[]
    while ingredientsLine:
        properties=ingredientsLine.split(",")
        #print(properties)
        while properties[0][0] == '#' or properties[0] == '\n':
            ingredientsLine=ingredientsFile.readline()
            properties=ingredientsLine.split(",")
        if category==0:
            if properties[0]=="flavour" or properties[0]=="nicotine concentrate":
                ingredientList.append(properties[3])
        else:
            if properties[0]==category:
                ingredientList.append(properties[3])
        ingredientsLine=ingredientsFile.readline()
    ingredientsFile.close()
    return(ingredientList)


def fetchNicotine(event):
    nic=nicotineNameLabel.get()
    ingredientfile=open("ingredients.txt","r")
    ingredientsLine=ingredientfile.readline()
    while ingredientsLine:
        properties=ingredientsLine.split(",")
        while properties[0][0] == '#' or properties[0] == '\n':
            ingredientsLine=ingredientfile.readline()
            properties=ingredientsLine.split(",")
        if properties[3]==nic:
            requiredingred=properties
        ingredientsLine=ingredientfile.readline()
    nicotineVg.set(requiredingred[1])
    nicotine.set(requiredingred[2])
    nicoCost=requiredingred[4]
    nicotineCost.set(nicoCost)
    ingredientfile.close()


def fetchFlavour1(event):
    flav=flavour1NameLabel.get()
    ingredientfile=open("ingredients.txt","r")
    ingredientsLine=ingredientfile.readline()
    while ingredientsLine:
        properties=ingredientsLine.split(",")
        while properties[0][0] == '#' or properties[0] == '\n':
            ingredientsLine=ingredientfile.readline()
            properties=ingredientsLine.split(",")
        if properties[3]==flav:
            requiredingred=properties
        ingredientsLine=ingredientfile.readline()
    flavour1.set(requiredingred[1])
    flavour1Cost.set(requiredingred[4])
    ingredientfile.close()


def fetchFlavour2(event):
    flav=flavour2NameLabel.get()
    ingredientfile=open("ingredients.txt","r")
    ingredientsLine=ingredientfile.readline()
    while ingredientsLine:
        properties=ingredientsLine.split(",")
        while properties[0][0] == '#' or properties[0] == '\n':
            ingredientsLine=ingredientfile.readline()
            properties=ingredientsLine.split(",")
        if properties[3]==flav:
            requiredingred=properties
        ingredientsLine=ingredientfile.readline()
    flavour2.set(requiredingred[1])
    flavour2Cost.set(requiredingred[4])
    ingredientfile.close()


def fetchFlavour3(event):
    flav=flavour3NameLabel.get()
    ingredientfile=open("ingredients.txt","r")
    ingredientsLine=ingredientfile.readline()
    while ingredientsLine:
        properties=ingredientsLine.split(",")
        while properties[0][0] == '#' or properties[0] == '\n':
            ingredientsLine=ingredientfile.readline()
            properties=ingredientsLine.split(",")
        if properties[3]==flav:
            requiredingred=properties
        ingredientsLine=ingredientfile.readline()
    flavour3.set(requiredingred[1])
    flavour3Cost.set(requiredingred[4])
    ingredientfile.close()

def cancel(window):
    window.destroy()

def okPrice(window,price,vg_or_pg):
    ingredientsFile=open("ingredients.txt","r")
    ingredientsLine=ingredientsFile.readline()
    ingredientsList=ingredientsLine.split(",")
    saveData=""
    while ingredientsLine:
        if ingredientsList[0]==vg_or_pg:
            saveData+=ingredientsList[0]+","+ingredientsList[1]+","+ingredientsList[2]+","+ingredientsList[3]+","+price+","+"\n"
        else:
            saveData+=ingredientsLine
        ingredientsLine=ingredientsFile.readline()
        ingredientsList=ingredientsLine.split(",")
    ingredientsFile.close()
    ingredientsFile=open("ingredients.txt","w")
    ingredientsFile.write(saveData)
    ingredientsFile.close()
    window.destroy()
    
    
def vgSet():
    vgWin=Toplevel(vapGui)
    vgWin.title("Set VG price")
    currentVgTextLabel=ttk.Label(vgWin,text="Current VG price=").grid(row=0,column=0,padx=10,pady=10,sticky=E)
    newVgTextLabel=ttk.Label(vgWin,text="New VG price=").grid(row=1,column=0,padx=10,pady=10,sticky=E)
    global newVgPrice
    newVgPrice=StringVar()
    newVgPrice.set(0)
    newVgPriceBox=ttk.Entry(vgWin,textvariable=newVgPrice).grid(row=1,column=1)
    unitLabel=ttk.Label(vgWin,text="£/ml").grid(row=0,column=2,padx=10,pady=10)
    unitLabel=ttk.Label(vgWin,text="£/ml").grid(row=1,column=2,padx=10,pady=10)
    ingredientFile=open("ingredients.txt","r")
    ingredientLine=ingredientFile.readline()
    ingredientList=ingredientLine.split(",")
    while ingredientList[0]!="vg":
        ingredientLine=ingredientFile.readline()
        ingredientList=ingredientLine.split(",")
    currentVgPriceLabel=ttk.Label(vgWin,text=ingredientList[4])
    currentVgPriceLabel.grid(row=0,column=1,padx=10,pady=10,sticky=W)
    newVgPrice.set("0")
    okFrame=ttk.Frame(vgWin)
    okButton=ttk.Button(okFrame,text="OK",command=lambda: okPrice(vgWin,newVgPrice.get(),"vg")).grid(row=0,column=0,padx=10,pady=10)
    cancelButton=ttk.Button(okFrame,text="Cancel",command=lambda: cancel(vgWin)).grid(row=0,column=1,padx=10,pady=10)
    okFrame.grid(row=2,columnspan=3)
    ingredientFile.close()

    
def pgSet():
    pgWin=Toplevel(vapGui)
    pgWin.title("Set PG price")
    currentPgTextLabel=ttk.Label(pgWin,text="Current PG price=").grid(row=0,column=0,padx=10,pady=10,sticky=E)
    newPgTextLabel=ttk.Label(pgWin,text="New PG price=").grid(row=1,column=0,padx=10,pady=10,sticky=E)
    global newPgPrice
    newPgPrice=StringVar()
    newPgPrice.set(0)
    newPgPriceBox=ttk.Entry(pgWin,textvariable=newPgPrice).grid(row=1,column=1)
    unitLabel=ttk.Label(pgWin,text="£/ml").grid(row=0,column=2,padx=10,pady=10)
    unitLabel=ttk.Label(pgWin,text="£/ml").grid(row=1,column=2,padx=10,pady=10)
    ingredientFile=open("ingredients.txt","r")
    ingredientLine=ingredientFile.readline()
    ingredientList=ingredientLine.split(",")
    while ingredientList[0]!="pg":
        ingredientLine=ingredientFile.readline()
        ingredientList=ingredientLine.split(",")
    currentPgPriceLabel=ttk.Label(pgWin,text=ingredientList[4])
    currentPgPriceLabel.grid(row=0,column=1,padx=10,pady=10,sticky=W)
    newPgPrice.set("0")
    okFrame=ttk.Frame(pgWin)
    okButton=ttk.Button(okFrame,text="OK",command=lambda: okPrice(pgWin,newPgPrice.get(),"pg")).grid(row=0,column=0,padx=10,pady=10)
    cancelButton=ttk.Button(okFrame,text="Cancel",command=lambda: cancel(pgWin)).grid(row=0,column=1,padx=10,pady=10)
    okFrame.grid(row=2,columnspan=3)
    ingredientFile.close()

def delete(category,item,filename,box):
    saveFile=open(filename,"r")
    saveLine=saveFile.readline()
    #print(saveLine)
    saveData=""
    if category=="ingredient":
        saveList=saveLine.split(",")
    else:
        saveList=saveLine.split()
    #print(saveList)
    while saveLine:
        while saveList[0][0]=="#" or saveList[0]=="\n":
            saveLine=saveFile.readline()
            #print(saveLine)
            if category=="ingredient":
                saveList=saveLine.split(",")
            else:
                saveList=saveLine.split()
            #print(saveList)
        if category=="ingredient":
            #print(saveList)
            if saveList[3]!=item:
                saveData+=saveLine
        else:
            if saveList[0]!=item:
                saveData+=saveLine
        saveLine=saveFile.readline()
        if category=="ingredient":
            saveList=saveLine.split(",")
        else:
            saveList=saveLine.split()
    saveFile.close()
    saveFile=open(filename,"w")
    saveFile.write(saveData)
    saveFile.close()
    box.destroy()
    
def deleteIngredient():
    DIWin=Toplevel(vapGui)
    DIWin.title("Ingredient Deletion")
    selectIngredientLabel=ttk.Label(DIWin,text="Please select ingredient for deletion from list",wraplength=200,justify=CENTER).grid(row=0,column=0,padx=10,pady=10)
    ingredientDelete=StringVar()
    ingredientDeleteBox=ttk.Combobox(DIWin,textvariable=ingredientDelete)
    ingredientDeleteBox['values']=getIngredients()
    ingredientDeleteBox.grid(row=1,column=0,padx=10,pady=10)
    okFrame=ttk.Frame(DIWin)
    okButton=ttk.Button(okFrame,text="Delete",command=lambda: delete("ingredient",ingredientDelete.get(),"ingredients.txt",DIWin)).grid(row=0,column=0,padx=10,pady=10)
    cancelButton=ttk.Button(okFrame,text="Cancel",command=lambda: cancel(DIWin)).grid(row=0,column=1,padx=10,pady=10)
    okFrame.grid(row=2,columnspan=2)
    

def deleteRecipe():
    DRWin=Toplevel(vapGui)
    DRWin.title("Recipe Deletion")
    selectRecipeLabel=ttk.Label(DRWin,text="Please select Recipe for deletion from list",wraplength=200,justify=CENTER).grid(row=0,column=0,padx=10,pady=10)
    recipeDelete=StringVar()
    recipeDeleteBox=ttk.Combobox(DRWin,textvariable=recipeDelete)
    recipeDeleteBox['values']=getRecipes()
    recipeDeleteBox.grid(row=1,column=0,padx=10,pady=10)
    okFrame=ttk.Frame(DRWin)
    okButton=ttk.Button(okFrame,text="Delete",command=lambda: delete("recipe",recipeDelete.get(),"recipes.txt",DRWin)).grid(row=0,column=0,padx=10,pady=10)
    cancelButton=ttk.Button(okFrame,text="Cancel",command=lambda: cancel(DRWin)).grid(row=0,column=1,padx=10,pady=10)
    okFrame.grid(row=2,columnspan=2)

    
    
vapGui=Tk()
vapGui.option_add('*tearOff', FALSE)
menubar=Menu(vapGui)
menu_file=Menu(menubar)
menubar.add_cascade(menu=menu_file, label='File')
menu_file.add_command(label='Change VG price', command=vgSet)
menu_file.add_command(label='Change PG price', command=pgSet)
menu_file.add_command(label='Delete ingredient', command=deleteIngredient)
menu_file.add_command(label='Delete recipe', command=deleteRecipe)
vapGui["menu"]=menubar
vapGui.option_add('*tearOff', FALSE)
vapGui.title("Vaping Calculator")
percentageVgLabel1=Label(vapGui,text="% vg in nicotine").grid(row=1,column=3)
percentageVgLabel2=Label(vapGui,text="% vg in Flavour").grid(row=4,column=3)
flavourPercentLabel=Label(vapGui,text="% of flavour required").grid(row=4,column=9,padx=(0,10))
labelFrame1=ttk.Frame(vapGui)
batchSizeLabel=Label(labelFrame1,text="Batch size ml").grid(row=0,column=1,padx=30)
NicotineConcLabel1=Label(vapGui,text="Nicotine conc in\n concentrate mg/ml").grid(row=1,column=9,padx=(0,10))
NicotineConcLabel2=Label(labelFrame1,text="Nicotine Conc of\n final product mg/ml").grid(row=0,column=0,padx=30)
nicotineNameLabel=StringVar()
nicotineNameLabel.set("Nicotine")
nicotineLabel=ttk.Combobox(vapGui,textvariable=nicotineNameLabel)
nicotineLabel["values"]=(getIngredients("nicotine concentrate"))
nicotineLabel.bind('<<ComboboxSelected>>',fetchNicotine)
nicotineLabel.grid(row=2,column=1,sticky=W,padx=(10,0))
flavour1NameLabel=StringVar()
flavour1NameLabel.set("Flavour 1")
flavour1Label=ttk.Combobox(vapGui,textvariable=flavour1NameLabel)
flavour1Label["values"]=(getIngredients("flavour"))
flavour1Label.bind('<<ComboboxSelected>>',fetchFlavour1)
flavour1Label.grid(row=5,column=1,sticky=W,padx=(10,0))
flavour2NameLabel=StringVar()
flavour2NameLabel.set("Flavour 2")
flavour2Label=ttk.Combobox(vapGui,textvariable=flavour2NameLabel)
flavour2Label["values"]=(getIngredients("flavour"))
flavour2Label.bind('<<ComboboxSelected>>',fetchFlavour2)
flavour2Label.grid(row=6,column=1,sticky=W,padx=(10,0))
flavour3NameLabel=StringVar()
flavour3NameLabel.set("Flavour 3")
flavour3Label=ttk.Combobox(vapGui,textvariable=flavour3NameLabel)
flavour3Label["values"]=(getIngredients("flavour"))
flavour3Label.bind('<<ComboboxSelected>>',fetchFlavour3)
flavour3Label.grid(row=7,column=1,sticky=W,padx=(10,0))
vgLabel=Label(vapGui,text="%age VG required").grid(row=10,columnspan=10)

blankLabel1=Label(vapGui,text="        ").grid(row=1,column=2)
blankLabel1=Label(vapGui,text="        ").grid(row=1,column=4)
blankLabel1=Label(vapGui,text="        ").grid(row=1,column=6)
blankLabel1=Label(vapGui,text="        ").grid(row=3,column=1)
blankLabel1=Label(vapGui,text="        ").grid(row=8,column=1)
#blankLabel1=Label(vapGui,text="        ").grid(row=1,column=8)
nicotineVg=StringVar()
nicotineVgText=Entry(vapGui,textvariable=nicotineVg).grid(row=2,column=3)
nicotineVg.set(100)
nicotine=StringVar()
nicotineText=Entry(vapGui,textvariable=nicotine).grid(row=2,column=9,padx=(0,10))
nicotine.set(72)
nicotineFinal=StringVar()
nicotineFinalText=Entry(labelFrame1,textvariable=nicotineFinal).grid(row=1,column=0,padx=50,pady=(0,20))
nicotineFinal.set(3)
flavour1=StringVar()
flavour1Text=Entry(vapGui, textvariable=flavour1).grid(row=5,column=3)
flavour1Percent=StringVar()
flavour1PercentText=Entry(vapGui, textvariable=flavour1Percent).grid(row=5,column=9,padx=(0,10))
flavour2=StringVar()
flavour2Text=Entry(vapGui, textvariable=flavour2).grid(row=6,column=3)
flavour2Percent=StringVar()
flavour2PercentText=Entry(vapGui, textvariable=flavour2Percent).grid(row=6,column=9,padx=(0,10))
flavour3=StringVar()
flavour3Text=Entry(vapGui, textvariable=flavour3).grid(row=7,column=3)
flavour3Percent=StringVar()
flavour3PercentText=Entry(vapGui, textvariable=flavour3Percent).grid(row=7,column=9,padx=(0,10))
flavour1.set(0)
flavour1Percent.set(0)
flavour2.set(0)
flavour2Percent.set(0)
flavour3.set(0)
flavour3Percent.set(0)
batchSize=StringVar()
batchSizeBox=Entry(labelFrame1,textvariable=batchSize).grid(row=1,column=1,padx=50,pady=(0,20))
batchSize.set(25)
labelFrame1.grid(row=9,columnspan=10)
nicotineCost=StringVar()
nicotineCostBox=ttk.Entry(vapGui,textvariable=nicotineCost).grid(row=2,column=5)
nicotineCost.set(0)
costlabel=ttk.Label(vapGui,text="price £/ml").grid(row=1,column=5)
costlabel=ttk.Label(vapGui,text="price £/ml").grid(row=4,column=5)
flavour1Cost=StringVar()
flavour1CostBox=ttk.Entry(vapGui,textvariable=flavour1Cost).grid(row=5,column=5)
flavour1Cost.set(0)
flavour2Cost=StringVar()
flavour2CostBox=ttk.Entry(vapGui,textvariable=flavour2Cost).grid(row=6,column=5)
flavour2Cost.set(0)
flavour3Cost=StringVar()
flavour3CostBox=ttk.Entry(vapGui,textvariable=flavour3Cost).grid(row=7,column=5)
flavour3Cost.set(0)
vgSlider=Scale(vapGui,from_=0, to=100,orient=HORIZONTAL,length=600)
vgSlider.set(50)
vgSlider.grid(columnspan=10,row=11)
output=Listbox(vapGui,height=7,width=70)
output.grid(columnspan=10,row=13)

boxes=[nicotine,nicotineFinal,nicotineVg,flavour1,flavour2,flavour3,flavour1Percent,flavour2Percent,flavour3Percent,vgSlider,batchSize,nicotineCost,flavour1Cost,flavour2Cost,flavour3Cost]

vapGui.columnconfigure(0,weight=1)
vapGui.columnconfigure(1,weight=1)
vapGui.columnconfigure(2,weight=1)
vapGui.columnconfigure(3,weight=1)
vapGui.columnconfigure(4,weight=1)
vapGui.columnconfigure(5,weight=1)
vapGui.columnconfigure(6,weight=1)
vapGui.columnconfigure(7,weight=1)
vapGui.columnconfigure(8,weight=1)
vapGui.columnconfigure(9,weight=1)
vapGui.rowconfigure(0,weight=1)
vapGui.rowconfigure(1,weight=1)
vapGui.rowconfigure(2,weight=1)
vapGui.rowconfigure(3,weight=1)
vapGui.rowconfigure(4,weight=1)
vapGui.rowconfigure(5,weight=1)
vapGui.rowconfigure(6,weight=1)
vapGui.rowconfigure(7,weight=1)
vapGui.rowconfigure(8,weight=1)
vapGui.rowconfigure(9,weight=1)
vapGui.rowconfigure(10,weight=1)
vapGui.rowconfigure(11,weight=1)
vapGui.rowconfigure(12,weight=1)
vapGui.rowconfigure(13,weight=1)

def getVariables(boxes):
    variables=[]
    for variable in boxes:
        variables.append(float(variable.get()))
    return(variables)
def main():
    data=getVariables(boxes)
    calculateValues(data)
    #calculated=calculateValues(data)
    

def calculateValues(data):
    nicotineConcInit=data[0]
    nicotineConcFinal=data[1]
    nicotineVg=data[2]
    flavour1Vg=data[3]
    flavour2Vg=data[4]
    flavour3Vg=data[5]
    flavour1Percent=data[6]
    flavour2Percent=data[7]
    flavour3Percent=data[8]
    vgPercent=data[9]
    batch=data[10]
    nicCost=data[11]
    f1Cost=data[12]
    f2Cost=data[13]
    f3Cost=data[14]
    calc=makeup(batch,nicotineConcInit,nicotineConcFinal,nicotineVg,vgPercent,flavour1Vg,flavour1Percent,flavour2Vg,flavour2Percent,flavour3Vg,flavour3Percent,nicCost,f1Cost,f2Cost,f3Cost)
    printBox(calc[0],calc[1])

def makeup(batchsize,nicotineConcInit,nicotineConc,nicotineVg,percentVg,flavour1Vg,flavourConc1,flavour2Vg,flavourConc2,flavour3Vg,flavourConc3,nicCost,f1Cost,f2Cost,f3Cost):
               
    nicotineVol=float(nicotineConc*batchsize)/float(nicotineConcInit)
    flavourVol1=(batchsize*flavourConc1*0.01)
    flavourVol2=(batchsize*flavourConc2*0.01)
    flavourVol3=(batchsize*flavourConc3*0.01)
    
    pgVolume=((flavourVol1*(100-flavour1Vg)+flavourVol2*
    (100-flavour2Vg)+flavourVol3*(100-flavour3Vg)+nicotineVol*(100-nicotineVg))/100)
    
    vgVolume=((flavourVol1*flavour1Vg+flavourVol2*
    flavour2Vg+flavourVol3*flavour3Vg+nicotineVol*
               nicotineVg)/100)   
    
    #print(vgVolume)    
    requiredPg=(float(100-percentVg)/100)*batchsize-pgVolume
    #print(requiredPg)
    requiredVg=(float(percentVg)/100)*batchsize-vgVolume
    #print(requiredVg)
    vgCost=pgVgCost()[0]
    pgCost=pgVgCost()[1]
    batchCost=nicotineVol*float(nicCost)+flavourVol1*float(f1Cost)+flavourVol2*float(f2Cost)+flavourVol3*float(f3Cost)+requiredPg*float(pgCost)+requiredVg*float(vgCost)
    return([[nicotineVol,flavourVol1,flavourVol2,flavourVol3,requiredVg,requiredPg],batchCost])

def pgVgCost():
    ingredientsfile=open("ingredients.txt","r")
    ingredientsLine=ingredientsfile.readline()
    ingredient=ingredientsLine.split(",")
    while ingredientsLine:
        if ingredient[0]=="vg":
            vgPrice=ingredient[4]
        if ingredient[0]=="pg":
            pgPrice=ingredient[4]
        ingredientsLine=ingredientsfile.readline()
        ingredient=ingredientsLine.split(",")    
    ingredientsfile.close()
    return([vgPrice,pgPrice])


def printBox(info,cost):
    output.delete(0,END)
    dictionary={}
    itemList=[]
    if info[0]!=0:
        itemList+=[[round(info[0],2),nicotineNameLabel.get()]]
    if info[1]!=0:
        itemList+=[[round(info[1],2),flavour1NameLabel.get()]]
    if info[2]!=0:
        itemList+=[[round(info[2],2),flavour2NameLabel.get()]]
    if info[3]!=0:
        itemList+=[[round(info[3],2),flavour3NameLabel.get()]]
    itemList+=[[round(info[4],2),"VG"]]
    itemList+=[[round(info[5],2),"PG"]]
    #print(itemList)
    itemList.sort(key=lambda row: row[0])
    for item in itemList:
        output.insert(END,("Volume of "+item[1]+" = "+str(item[0])))
    
    output.insert(END,("Price for the batch is £"+str(round(cost,2))))

def getRecipes():
    recipeFile=open("recipes.txt","r")
    aline=recipeFile.readline()
    recipeList=[]
    while aline:
        values=aline.split()
        recipeList+=[values[0]]
        aline=recipeFile.readline()
    return(recipeList)
    

def loadSelected(*arg):
    selection=recipe.get()
    #print(selection)
    recipeFile=open("recipes.txt","r")
    aline=recipeFile.readline()
    recipeData=aline.split()
    #print(recipeData)
    while recipeData[0]!=selection:
        aline=recipeFile.readline()
        recipeData=aline.split()
    assembleData(recipeData)

def getIngredientLine(item):
    ingredientsFile=open("ingredients.txt","r")
    ingredientline=ingredientsFile.readline()
    ingredient=ingredientline.split(",")
    while ingredient[0][0]=="#" or ingredientline=="\n":
            ingredientline=ingredientsFile.readline()
            ingredient=ingredientline.split(",")
        #print(ingredientline)
    while item!=ingredient[3]:
        ingredientline=ingredientsFile.readline()
        ingredient=ingredientline.split(",")
        while ingredientline[0]=="#" or ingredientline=="\n":
            ingredientline=ingredientsFile.readline()
            ingredient=ingredientline.split(",")
    return(ingredient)

def assembleData(recipeData):
    ingredientsFile=open("ingredients.txt","r")
    ingredientline=ingredientsFile.readline()
    ingredient=ingredientline.split(",")
    warning=0
    ingredientList=[]
    while ingredientline:
        while ingredient[0][0]=="#" or ingredientline=="\n":
            ingredientline=ingredientsFile.readline()
            ingredient=ingredientline.split(",")
        ingredientList+=[ingredient[3]]
        ingredientline=ingredientsFile.readline()
        ingredient=ingredientline.split(",")
        
    if recipeData[1] not in ingredientList:
        nicotineVgget=0
        nicotineInitialConcget=72
        nicotineNameget="Nicotine"
        nicotinePriceget=0
        warning=1
    else:
        ingredient=getIngredientLine(recipeData[1])
        nicotinePriceget=0
        nicotineVgget=ingredient[1]
        nicotineInitialConcget=ingredient[2]
        nicotineNameget=ingredient[3]
        nicotinePriceget=ingredient[4]
        
    recipeNicotineConcget=recipeData[2]
    recipeVgget=recipeData[3]
    if recipeData[4] not in ingredientList:
        flavour1Vgget=0
        flavour1Nameget=recipeData[3]
        flavour1Priceget=0
        warning=1
    else:
        ingredient=getIngredientLine(recipeData[4])
        flavour1Vgget=ingredient[1]
        flavour1Nameget=ingredient[3]
        flavour1Priceget=ingredient[4]
        
    flavour1Concget=recipeData[5]
    try:
        recipeData[6]
    except IndexError:
        flavour2Nameget="Flavour 2"
        flavour2Vgget=0
        flavour2Concget=0
        flavour2Priceget=0
        flavour3Nameget="Flavour 3"
        flavour3Vgget=0
        flavour3Concget=0
        flavour3Priceget=0
        if warning==1:
            messagebox.showinfo(message="Some ingredients required for the recipe are not currently saved, so details may be incorrect",title="Warning")
        inputSet(nicotineVgget,nicotineInitialConcget,nicotineNameget,nicotinePriceget,recipeNicotineConcget,recipeVgget,flavour1Vgget,flavour1Nameget,flavour1Priceget,flavour1Concget,flavour2Vgget,flavour2Nameget,flavour2Priceget,flavour2Concget,flavour3Vgget,flavour3Nameget,flavour3Priceget,flavour3Concget)
        return
    if recipeData[6] not in ingredientList:
        flavour2Vgget=0
        flavour2Nameget=recipeData[6]
        flavour2Priceget=0
        warning=1
    else:
        ingredient=getIngredientLine(recipeData[6])        
        flavour2Vgget=ingredient[1]
        flavour2Nameget=ingredient[3]
        flavour2Priceget=ingredient[4]
    flavour2Concget=recipeData[7]    
    try:
        recipeData[8]
    except IndexError:
        flavour3Nameget="Flavour 3"
        flavour3Vgget=0
        flavour3Concget=0
        flavour3Priceget=0
        if warning==1:
            messagebox.showinfo(message="Some ingredients required for the recipe are not currently saved, so details may be incorrect",title="Warning")
        inputSet(nicotineVgget,nicotineInitialConcget,nicotineNameget,nicotinePriceget,recipeNicotineConcget,recipeVgget,flavour1Vgget,flavour1Nameget,flavour1Priceget,flavour1Concget,flavour2Vgget,flavour2Nameget,flavour2Priceget,flavour2Concget,flavour3Vgget,flavour3Nameget,flavour3Priceget,flavour3Concget)
        return
    if recipeData[8] not in ingredientList:
        flavour3Vgget=0
        flavour3Nameget=recipeData[8]
        flavour3Priceget=0
        warning=1
    else:
        ingredient=getIngredientLine(recipeData[8])        
        flavour3Vgget=ingredient[1]
        flavour3Nameget=ingredient[3]
        flavour3Priceget=ingredient[4]
    flavour3Concget=recipeData[9]
    if warning==1:
        messagebox.showinfo(message="Some ingredients required for the recipe are not currently saved, so details may be incorrect",title="Warning")
    inputSet(nicotineVgget,nicotineInitialConcget,nicotineNameget,nicotinePriceget,recipeNicotineConcget,recipeVgget,flavour1Vgget,flavour1Nameget,flavour1Priceget,flavour1Concget,flavour2Vgget,flavour2Nameget,flavour2Priceget,flavour2Concget,flavour3Vgget,flavour3Nameget,flavour3Priceget,flavour3Concget)
    return
    
def inputSet(nicotineVgget,nicotineInitialConcget,nicotineNameget,nicotinePriceget,recipeNicotineConcget,recipeVgget,flavour1Vgget,flavour1Nameget,flavour1Priceget,flavour1Concget,flavour2Vgget,flavour2Nameget,flavour2Priceget,flavour2Concget,flavour3Vgget,flavour3Nameget,flavour3Priceget,flavour3Concget):
    nicotineVg.set(nicotineVgget)
    nicotine.set(nicotineInitialConcget)
    nicotineNameLabel.set(nicotineNameget)
    nicotineFinal.set(recipeNicotineConcget)
    vgSlider.set(recipeVgget)
    flavour1.set(flavour1Vgget)
    flavour1NameLabel.set(flavour1Nameget)
    flavour1Percent.set(flavour1Concget)
    flavour2.set(flavour2Vgget)
    flavour2NameLabel.set(flavour2Nameget)
    flavour2Percent.set(flavour2Concget)
    flavour3.set(flavour3Vgget)
    flavour3NameLabel.set(flavour3Nameget)
    flavour3Percent.set(flavour3Concget)
    nicotineCost.set(nicotinePriceget)
    flavour1Cost.set(flavour1Priceget)
    flavour2Cost.set(flavour2Priceget)
    flavour3Cost.set(flavour3Priceget)

def saveIngredients():
    nicotineSaved=0
    flavour1Saved=0
    flavour2Saved=0
    flavour3Saved=0

    ingredients=[]
    if nicotineNameLabel.get()!="Nicotine":
        ingredients.append(nicotineNameLabel.get())
    else:
        messagebox.showinfo(message="you must enter name for nicotine then reattempt save",title="Warning")
        return(1)
    if flavour1NameLabel.get()=="Flavour 1" and flavour1Percent.get()!="0":
        messagebox.showinfo(message="you must enter name for flavour 1 then reattempt save",title="Warning")
        return(1)
    elif flavour1NameLabel.get()!="Flavour 1":
        ingredients.append(flavour1NameLabel.get())
    else:
        flavour1Saved=1
        
    if flavour2NameLabel.get()=="Flavour 2" and flavour2Percent.get()!="0":
        messagebox.showinfo(message="you must enter name for flavour 2 then reattempt save",title="Warning")
        return(1)
    elif flavour2NameLabel.get()!="Flavour 2":
        ingredients.append(flavour2NameLabel.get())
    else:
        flavour2Saved=1
        
    if flavour3NameLabel.get()=="Flavour 3" and flavour3Percent.get()!="0":
        messagebox.showinfo(message="you must enter name for flavour 3 then reattempt save",title="Warning")
        return(1)
    elif flavour3NameLabel.get()!="Flavour 3":
        ingredients.append(flavour3NameLabel.get())
    else:
        flavour3Saved=1

    ingredientsFile=open("ingredients.txt","r")
    ingredientsLine=ingredientsFile.readline()
    ingredientsData=ingredientsLine.split(",")
    overwrite=0
    while ingredientsLine and overwrite==0:
        while ingredientsData[0][0] == '#' or ingredientsData[0] == '\n':
            ingredientsLine=ingredientsFile.readline()
            ingredientsData=ingredientsLine.split(",")
        if ingredientsData[3]==nicotineNameLabel.get():
            if ingredientsData[0]!="nicotine concentrate" or ingredientsData[1]!=nicotineVg.get() or ingredientsData[2]!=nicotine.get() or ingredientsData[4]!=nicotineCost.get():
                value=messagebox.askokcancel(message="Changes to existing ingredients detected",icon="question",title="Warning")
                if value==False:
                    return(1)
                overwrite=1
        if ingredientsData[3]==flavour1NameLabel.get():
            if ingredientsData[0]!="flavour" or ingredientsData[1]!=flavour1.get() or ingredientsData[2]!="0" or ingredientsData[4]!=flavour1Cost.get():
                value=messagebox.askokcancel(message="Changes to existing ingredients detected",icon="question",title="Warning")
                if value==False:
                    return(1)
                overwrite=1

        if ingredientsData[3]==flavour2NameLabel.get():
            if ingredientsData[0]!="flavour" or ingredientsData[1]!=flavour2.get() or ingredientsData[2]!="0" or ingredientsData[4]!=flavour2Cost.get():
                value=messagebox.askokcancel(message="Changes to existing ingredients detected",icon="question",title="Warning")
                if value==False:
                    return(1)
                overwrite=1

        if ingredientsData[3]==flavour3NameLabel.get():
            if ingredientsData[0]!="flavour" or ingredientsData[1]!=flavour3.get() or ingredientsData[2]!="0" or ingredientsData[4]!=flavour3Cost.get():
                value=messagebox.askokcancel(message="Changes to existing ingredients detected",icon="question",title="Warning")
                if value==False:
                    return(1)
                overwrite=1
                
        ingredientsLine=ingredientsFile.readline()
        ingredientsData=ingredientsLine.split(",")
    saveData=""
    ingredientsFile=open("ingredients.txt","r")
    ingredientsLine=ingredientsFile.readline()
    ingredientsData=ingredientsLine.split(",")

    while ingredientsLine:
        while ingredientsData[0][0] == '#' or ingredientsData[0] == '\n':
            ingredientsLine=ingredientsFile.readline()
            ingredientsData=ingredientsLine.split(",")
        if ingredientsData[3]==nicotineNameLabel.get():
            saveData+="nicotine concentrate,"+str(nicotineVg.get())+","+str(nicotine.get())+","+str(nicotineNameLabel.get())+","+str(nicotineCost.get())+",\n"
            nicotineSaved=1
            
        elif ingredientsData[3]==flavour1NameLabel.get():
            saveData+="flavour,"+str(flavour1.get())+",0,"+str(flavour1NameLabel.get())+","+str(flavour1Cost.get())+",\n"
            flavour1Saved=1
            if ingredientsData[3]==flavour2NameLabel.get():
                flavour2Saved=1
                if  ingredientsData[3]==flavour3NameLabel.get():
                    flavour3Saved=1
            
        elif ingredientsData[3]==flavour2NameLabel.get():
            saveData+="flavour,"+str(flavour2.get())+",0,"+str(flavour2NameLabel.get())+","+str(flavour2Cost.get())+",\n"
            flavour2Saved=1
            if  ingredientsData[3]==flavour3NameLabel.get():
                flavour3Saved=1
            
        elif ingredientsData[3]==flavour3NameLabel.get():
            saveData+="flavour,"+str(flavour3.get())+",0,"+str(flavour3NameLabel.get())+","+str(flavour3Cost.get())+",\n"
            flavour3Saved=1
        else:
            saveData+=ingredientsLine
        ingredientsLine=ingredientsFile.readline()
        ingredientsData=ingredientsLine.split(",")
        

    if nicotineSaved==0:
        saveData+="nicotine concentrate,"+str(nicotineVg.get())+","+str(nicotine.get())+","+str(nicotineNameLabel.get())+","+str(nicotineCost.get())+",\n"
    if flavour1Saved==0:
        saveData+="flavour,"+str(flavour1.get())+",0,"+str(flavour1NameLabel.get())+","+str(flavour1Cost.get())+",\n"
    if flavour2Saved==0:
        saveData+="flavour,"+str(flavour2.get())+",0,"+str(flavour2NameLabel.get())+","+str(flavour2Cost.get())+",\n"
    if flavour3Saved==0:
        saveData+="flavour,"+str(flavour3.get())+",0,"+str(flavour3NameLabel.get())+","+str(flavour3Cost.get())+",\n"
    ingredientsFile.close()
    ingredientsFile=open("ingredients.txt","w")
    ingredientsFile.write(saveData)
    ingredientsFile.close()
    return(0)
            
def saveRecipe():
    choice=recipe.get()
    if choice=="":
        messagebox.showinfo(message="you must enter name for Recipe then reattempt save",title="Warning")
        return()
    proceed=saveIngredients()
    if proceed==1:
        return()
    recipeFile=open("recipes.txt","r")
    saveData=""
    recipeLine=recipeFile.readline()
    recipeData=recipeLine.split()
    recipeSaved=0
    while recipeLine:
        if recipeData[0]==choice:
            value=messagebox.askokcancel(message="Are you sure you wish to save over an existing recipe?",icon="question",title="Warning")
            if value==False:
                return()
            else:
                saveData+=choice+" "+nicotineNameLabel.get()+" "+str(nicotineFinal.get())+" "+str(vgSlider.get())
                if flavour1NameLabel.get()!="Flavour 1":
                    saveData+=" "+flavour1NameLabel.get()+" "+str(flavour1Percent.get())
                if flavour2NameLabel.get()!="Flavour 2":
                    saveData+=" "+flavour2NameLabel.get()+" "+str(flavour2Percent.get())
                if flavour3NameLabel.get()!="Flavour 3":
                    saveData+=" "+flavour3NameLabel.get()+" "+str(flavour3Percent.get())+"\n"
                else:
                    saveData+="\n"
        
                recipeSaved=1
        else:
            saveData+=recipeLine
        recipeLine=recipeFile.readline()
        recipeData=recipeLine.split()
    if recipeSaved==0:
        saveData+=choice+" "+nicotineNameLabel.get()+" "+str(nicotineFinal.get())+" "+str(vgSlider.get())
        if flavour2NameLabel.get()!="Flavour 1":
            saveData+=" "+flavour1NameLabel.get()+" "+str(flavour1Percent.get())
        if flavour2NameLabel.get()!="Flavour 2":
            saveData+=" "+flavour2NameLabel.get()+" "+str(flavour2Percent.get())
        if flavour3NameLabel.get()!="Flavour 3":
            saveData+=" "+flavour3NameLabel.get()+" "+str(flavour3Percent.get())+"\n"
        else:
            saveData+="\n"
            
    recipeFile.close()
    saveFile=open("recipes.txt","w")
    saveFile.write(saveData)
    saveFile.close()
            
                  
def recipeLoad():
    loadFrame=ttk.Frame(vapGui,relief="sunken",borderwidth=2)
    recipeLabel=ttk.Label(loadFrame,text="Select Recipe").grid(row=0,column=0,pady=5)
    global recipe
    recipe=StringVar()
    recipeBox=ttk.Combobox(loadFrame,textvariable=recipe)
    recipeBox['values'] = getRecipes()
    recipeBox.bind('<<ComboboxSelected>>',loadSelected)
    recipeBox.grid(row=1,column=0,padx=20,pady=5)
    saveButton=ttk.Button(loadFrame,text="Save Recipe and Ingredients",command=saveRecipe).grid(row=0,column=1,sticky=(E,W,N,S),padx=10)
    clearButton=ttk.Button(loadFrame,text="Clear Selection",command= lambda: inputSet(100,72,"Nicotine",0,3,50,0,"Flavour 1",0,0,0,"Flavour 2",0,0,0,"Flavour 3",0,0)).grid(row=1,column=1,sticky=(E,W),padx=10)
    loadFrame.grid(row=0,columnspan=10,pady=10)
        
buttonFrame=ttk.Frame(vapGui)
calcButton=ttk.Button(buttonFrame,text="Press to Calculate",command=main).grid(row=12,column=1,padx=2)
recipeButton=ttk.Button(buttonFrame, text="Save/Load Recipes",command=recipeLoad).grid(row=12,column=0,padx=2)
#saveButto=ttk.Button(buttonFrame, text="Save Current Recipe",command=saveRecipe).grid(row=12,column=2,padx=2)
buttonFrame.grid(row=12,columnspan=10)
mainloop()
