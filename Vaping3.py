class Vaping:
    def __init__(self,category,percentvg,gperlnicotine,name,priceperml):
    #setting up a class with all the information required
        self.category=category       
        self.percentvg=percentvg       
        self.percentpg=100-percentvg   
        self.nicotine=gperlnicotine                     
        self.name=name
        self.priceperml=priceperml
        
    def __str__(self):#create a statement that will be called if the object is printed
        if self.category=="nicotine concentrate":
            return("This is a "+self.category+" called "+self.name+"\nwith "+str(self.percentvg)+
            "%vg "+str(self.percentpg)+"% pg \nand a nicotine concentration of "
            +str(self.nicotine)+"mg/ml")
        else:
            return("This is a "+self.category+" called "+self.name+"\nwith "+str(self.percentvg)+
            "%vg and "+str(self.percentpg)+"% pg ")
            
    def getNicotine(self):#gets nicotine concentration
        return(float(self.nicotine))

    def getCategory(self):#gets the class of ingredient
        return(self.category)

    def getVg(self):#get %age of vg in ingredient
        return(float(self.percentvg))

    def getPg(self):#get %age of pg in ingredient
        return(float(self.percentpg))
        
    def getName(self):#get the name of the ingredient
        return(self.name)

    def getPrice(self):#get the cost of additive in £/ml
        return(float(self.priceperml))

def importIngredients():
    ingredientsFile=open("ingredients.txt","r")
    ingredientsLine=ingredientsFile.readline()
    ingredientList=[]
    while ingredientsLine:
        properties=ingredientsLine.split(",")
        #print(properties)
        if properties[0][0] != '#' and properties[0] != '\n':
            ingredientList.append(Vaping(properties[0],float(properties[1]),float(properties[2]),properties[3],float(properties[4])))
        ingredientsLine=ingredientsFile.readline()
    ingredientsFile.close()
    return(ingredientList)

def getAvailable(ingredients,category):
    available=[]
    for ingredient in ingredients:
        cat=ingredient.getCategory()
        if cat==category:
            current=(ingredient.getName())
            print(current)
            available=available+[current]
    return(available)

def getIngredientData(ingredients,name):
    for ingredient in ingredients:
        if ingredient.getName()==name:
            data=[ingredient.getVg(),ingredient.getPg(),ingredient.getNicotine(),ingredient.getName(),ingredient.getPrice()]
            return(data)  

def makeup(ingredients,batchsize,nicotineName,nicotineConc,percentVg,flavourName1,
           flavourConc1,flavourName2="default",flavourConc2=0,
           flavourName3="default",flavourConc3=0):
    nicotineData=getIngredientData(ingredients,nicotineName)
    flavourData1=getIngredientData(ingredients,flavourName1)
    flavourData2=getIngredientData(ingredients,flavourName2)
    flavourData3=getIngredientData(ingredients,flavourName3)
    vgData=getIngredientData(ingredients,"vg")
    pgData=getIngredientData(ingredients,"pg")
    nicotineVol=float(nicotineConc*batchsize)/float(nicotineData[2])
    flavourVol1=(batchsize*flavourConc1*0.01)
    flavourVol2=(batchsize*flavourConc2*0.01)
    flavourVol3=(batchsize*flavourConc3*0.01)
    
    pgVolume=((flavourVol1*flavourData1[1]+flavourVol2*
    flavourData2[1]+flavourVol3*flavourData3[1]+nicotineVol*
               nicotineData[1])/100)

    vgVolume=((flavourVol1*flavourData1[0]+flavourVol2*
    flavourData2[0]+flavourVol3*flavourData3[0]+nicotineVol*
               nicotineData[0])/100)
    
    #print(vgVolume)    
    requiredPg=(float(100-percentVg)/100)*batchsize-pgVolume
    #print(requiredPg)
    requiredVg=(float(percentVg)/100)*batchsize-vgVolume
    #print(requiredVg)
    
    if flavourVol3!=0:
        print(nicotineData[3]+" volume\n="+str(nicotineVol)+
              "\n"+flavourData1[3]+" volume\n="+str(flavourVol1)+
              "\n"+flavourData2[3]+" volume\n="+str(flavourVol2)+
              "\n"+flavourData3[3]+" volume\n="+str(flavourVol3)+
              "\n"+"VG volume\n="+str(requiredVg)+"\n"+"PG volume\n="
              +str(requiredPg))
        
        print("price for batch is £"+str(nicotineData[4]*nicotineVol+
        flavourData1[4]*flavourVol1+flavourData2[4]*flavourVol2
        +flavourData3[4]*flavourVol3+vgData[4]*requiredVg+
        pgData[4]*requiredPg))
                                         
    elif flavourVol2!=0:
        print(nicotineData[3]+" volume\n="+str(nicotineVol)+"\n"+
        flavourData1[3]+" volume\n="+str(flavourVol1)+"\n"+
        flavourData2[3]+" volume\n="+str(flavourVol2)+"\n"+
        "VG volume\n="+str(requiredVg)+"\n"+"PG volume\n="+
              str(requiredPg))

        print("price for batch is £"+str(nicotineData[4]*nicotineVol+
        flavourData1[4]*flavourVol1+flavourData2[4]*flavourVol2
        +vgData[4]*requiredVg+pgData[4]*requiredPg))
              
    else:
        print((nicotineData[3])+" volume\n="+str(nicotineVol)+"\n"+
              flavourData1[3]+" volume\n="+str(flavourVol1)+
              "\nVG volume\n="+str(requiredVg)+"\nPG volume\n="+
              str(requiredPg))
        
        print("price for batch is £"+str(nicotineData[4]*nicotineVol
                                         +flavourData1[4]*flavourVol1
                                         +vgData[4]*requiredVg+pgData[4]
                                         *requiredPg))

        
def getData(ingredients):
    existing=yesNo("do you want to use an existing recipe ")
    if existing=="y":
        data=readRecipes()
        if len(data)<7:
            makeup(ingredients,float(data[0]),data[1],float(data[2]),
                   float(data[3]),data[4],float(data[5]))
        elif len(data)<9:
            makeup(ingredients,float(data[0]),data[1],float(data[2]) ,float(data[3]),data[4],float(data[5])
            ,data[6],float(data[7]))
        else:
            makeup(ingredients,float(data[0]),data[1],float(data[2])
            ,float(data[3]),data[4],float(data[5])
            ,data[6],float(data[7]),data[8]
            ,float(data[9]))
        
    else:
        nicotineChoice=userInput("nicotine concentrate",ingredients)
        nicotineConcChoice=numberInput("""what concentration of nicotine do you want 
        in the made up juice in mg/ml""")
        vgChoice=numberInput("what percent of vg do you want")
        batchChoice=numberInput("how many ml do you want")
        flavourChoice1=userInput("flavour",ingredients)
        flavourConcChoice1=numberInput("what percentage of this flavour do you want")
        flavourChoice2=userInput("flavour",ingredients,2)
        if flavourChoice2=="n":
            makeup(ingredients,float(batchChoice),nicotineChoice
            ,float(nicotineConcChoice),float(vgChoice)
            ,flavourChoice1,float(flavourConcChoice1))
            
            saveChoice=input("""if you wish to save this recipe enter a recipe
            name otherwise enter n?\n""")
            if saveChoice!="n":
                saveRecipe([saveChoice,nicotineChoice,float(nicotineConcChoice)
                ,float(vgChoice),flavourChoice1,float(flavourConcChoice1)])
            
        else:
            flavourConcChoice2=numberInput("""what percentage of this flavour do
            you want\n""")
            flavourChoice3=userInput("flavour",ingredients,3)
            if flavourChoice3=="n":
                makeup(ingredients,float(batchChoice),nicotineChoice
                ,float(nicotineConcChoice),float(vgChoice)
                ,flavourChoice1,float(flavourConcChoice1)
                ,flavourChoice2,float(flavourConcChoice2))
                
                saveChoice=input("""if you wish to save this recipe enter a 
                recipe name otherwise enter n?\n""")
                
                if saveChoice!="n":
                    saveRecipe([saveChoice,nicotineChoice
                    ,float(nicotineConcChoice),float(vgChoice),flavourChoice1
                    ,float(flavourConcChoice1),flavourChoice2
                    ,float(flavourConcChoice2)])     
            else:
                flavourConcChoice3=numberInput("""what percentage of this flavour 
                do you want\n""")
                
                makeup(ingredients,float(batchChoice),nicotineChoice
                ,float(nicotineConcChoice),float(vgChoice)
                ,flavourChoice1,float(flavourConcChoice1)
                ,flavourChoice2,float(flavourConcChoice2)
                ,flavourChoice3,float(flavourConcChoice3))
                
                saveChoice=input("""if you wish to save this recipe enter a 
                recipe name otherwise enter n?\n""")
                
                if saveChoice!="n":
                    saveRecipe([saveChoice,nicotineChoice
                    ,float(nicotineConcChoice),float(vgChoice),flavourChoice1
                    ,float(flavourConcChoice1),flavourChoice2
                    ,float(flavourConcChoice2),flavourChoice3
                    ,float(flavourConcChoice3)])
                
def readRecipes():
    recipeFile=open("recipes.txt","r")
    aline=recipeFile.readline()
    #aline=recipeFile.readline()
    recipeList=[]
    print("available recipes are:")
    while aline:
        values=aline.split()
        print(values[0])
        recipeList+=[values[0]]
        aline=recipeFile.readline()
    recipeFile=open("recipes.txt","r")
    #print(recipeList)
    aline=recipeFile.readline()
    recipeChoice=input("Which recipe do you want\n")
    while recipeChoice not in recipeList:
        recipeChoice=input("invalid recipe please re-enter\n")

    recipe=aline.split()
    while recipe[0]!=recipeChoice:
        aline=recipeFile.readline()
        recipe=aline.split()
    recipe=recipe[1:]
    batchVol=input("what batch size do you want?\n")
    data=(batchVol,)
    for item in recipe:
        itemTup=(item,)
        data=data+itemTup
    #print(data)
    return(data)

def saveRecipe(saveList):
    stringlist=[]
    for item in saveList:
        stringItem=str(item)
        stringlist.append(stringItem)
    #print(stringlist)
    glue=" "
    saveString=glue.join(stringlist)
    #print(saveString)
    recipeFile=open("recipes.txt","r")
    recipeLine=recipeFile.readline()
    existingRecipes=""
    while recipeLine:
        existingRecipes+=recipeLine
        recipeLine=recipeFile.readline()
    recipeFile.close()
    saveFile=open("recipes.txt","w")
    saveFile.write(existingRecipes+"\n"+saveString)
    saveFile.close()

    
def userInput(category,ingredients,number=1):
    if category=="flavour" and number>1:
        print("please enter  "+category+" number "+str(number)+
              ", available options are:")
        options=getAvailable(ingredients,category)
        selection=input("if no further flavours are required enter n\n")
        if selection=="n":
            return(selection)
    else:
        print("please enter required "+category+", available options are:")
        options=getAvailable(ingredients,category)
        selection=input()
    while selection not in options:
        selection=input("""invalid choice, please enter valid option from 
        available\n""")
    return(selection)
    
def yesNo(text):
    choice=input(text+"y/n?\n")
    while choice!="y" and choice!="n":
        choice=input("please enter valid choice\n")
    return(choice)

def numberInput(text):
    print(text)
    number=input()
    whileExit=0
    while whileExit==0:
        try:
            numTest=float(number)
            whileExit=1
        except ValueError:
            print("please enter a number")
            number=input()
    return(number)
    
    
def main():
    ingredients=(importIngredients())
    getData(ingredients)
    
#getAvailable("flavour")
main()
#readRecipes()
#saveRecipe([2,3,"string"])
#makeup(standard,0.5,50,50,heisenberg,10)
#makeup(nicotine,3,50,20,heisenberg,10,menthol,5)
